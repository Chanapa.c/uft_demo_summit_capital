Public RegisterFileData 

'*********************************** START FUNCION WEB *****************************************************

'SUMMITCAPITALWEB_WebEdit
Public Function SUMMITCAPITALWEB_WebEdit(strTransName, objBrowser, objPage, objWebEdit, strValue)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	If strValue <> "" Then
		With Browser(objBrowser).Page(objPage)
			If .WebEdit(objWebEdit).Exist(60) Then
				.WebEdit(objWebEdit).WaitProperty "visible", True, 60000  'width enabled
				.WebEdit(objWebEdit).Set strValue	 
				strSuccess = "พบข้อมูล " & strValue & " บนหน้าจอ"
				Call FW_PrintInfo("micPass", strValue, " Expected Result : " & strSuccess & " PASS")  
    			strStatus = "Pass"
    			'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebEdit " & strStatus & " : " & strSuccess, "")
    			Err.Clear					
			Else
				strBusinessError = "ไม่พบข้อมูล " & objWebEdit & " บนหน้าจอ"
				strTechnicalError = Err.Description
				Call FW_PrintInfo("micFail", objWebEdit, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
   			    strStatus = "Fail"
   			    strResultStatus = "Fail"
   			    'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebEdit " & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
				Err.Clear
			End If
		End With
	End If
End Function

'SUMMITCAPITALWEB_WebButton
Public Function SUMMITCAPITALWEB_WebButton(strTransName, objoBrowser, objPage, objWebButton)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	With Browser(objoBrowser).Page(objPage)
		If .WebButton(objWebButton).Exist(60) Then
			.WebButton(objWebButton).WaitProperty "visible", True, 60000
			.WebButton(objWebButton).Click	 
			strSuccess = "พบข้อมูล " & objWebButton & " บนหน้าจอ"
			Call FW_PrintInfo("micPass", objWebButton, " Expected Result : " & strSuccess & " PASS")  
			strStatus = "Pass"
			'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebButton " & strStatus & " : " & strSuccess, "")
			Err.Clear					
		Else
			strBusinessError = "ไม่พบข้อมูล " & objWebButton & " บนหน้าจอ"
			strTechnicalError = Err.Description
			Call FW_PrintInfo("micFail", objWebButton, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
		    strStatus = "Fail"
		    strResultStatus = "Fail"
		   'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebButton " & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
			Err.Clear
		End If
	End With
End Function

'SUMMITCAPITALWEB_WebElement
Public Function SUMMITCAPITALWEB_WebElement(strTransName, objoBrowser, objPage, objWebElement)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	With Browser(objoBrowser).Page(objPage)
		If .WebElement(objWebElement).Exist(60) Then
			.WebElement(objWebElement).WaitProperty "visible", True, 60000
			.WebElement(objWebElement).Click	 
			strSuccess = "พบข้อมูล " & objWebElement & " บนหน้าจอ"
			Call FW_PrintInfo("micPass", objWebElement, " Expected Result : " & strSuccess & " PASS")  
			strStatus = "Pass"
			'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebElement " & strStatus & " : " & strSuccess, "")
			Err.Clear					
		Else
			strBusinessError = "ไม่พบข้อมูล " & objWebElement & " บนหน้าจอ"
			strTechnicalError = Err.Description
			Call FW_PrintInfo("micFail", objWebElement, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
		    strStatus = "Fail"
		    strResultStatus = "Fail"
		   'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebElement " & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
			Err.Clear
		End If
	End With
End Function

Public Function SUMMITCAPITALWEB_WebLink(strTransName, objoBrowser, objPage, objWebLink)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	With Browser(objoBrowser).Page(objPage)
		If .Link(objWebLink).Exist(60) Then
			.Link(objWebLink).WaitProperty "visible", True, 60000
			.Link(objWebLink).Click	 
			strSuccess = "พบข้อมูล " & objWebLink & " บนหน้าจอ"
			Call FW_PrintInfo("micPass", objWebLink, " Expected Result : " & strSuccess & " PASS")  
			strStatus = "Pass"
			'Call FW_WriteLog("Capture CMSWEB_WebELink " & strStatus & " : " & strSuccess, "")
			Err.Clear					
		Else
			strBusinessError = "ไม่พบข้อมูล " & objWebLink & " บนหน้าจอ"
			strTechnicalError = Err.Description
			Call FW_PrintInfo("micFail", objWebLink, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
		    strStatus = "Fail"
		    strResultStatus = "Fail"
		   ' Call FW_WriteLog("Capture CMSWEB_WebELink " & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
			Err.Clear
		End If
	End With
End Function

'SUMMITCAPITALWEB_WebList
Public Function SUMMITCAPITALWEB_WebList(strTransName, objBrowser, objPage, objWebList, strValue)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	If strValue <> "" Then
		With Browser(objBrowser).Page(objPage)
			If .WebList(objWebList).Exist(60) Then
				.WebList(objWebList).WaitProperty "visible", True, 60000  'width enabled
				.WebList(objWebList).Select strValue	 
				strSuccess = "พบข้อมูล " & strValue & " บนหน้าจอ"
				Call FW_PrintInfo("micPass", strValue, " Expected Result : " & strSuccess & " PASS")  
    			strStatus = "Pass"
    			'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebList " & strStatus & " : " & strSuccess, "")
    			Err.Clear					
			Else
				strBusinessError = "ไม่พบข้อมูล " & objWebList & " บนหน้าจอ"
				strTechnicalError = Err.Description
				Call FW_PrintInfo("micFail", objWebList, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
   			    strStatus = "Fail"
   			    strResultStatus = "Fail"
   			    'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebList" & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
				Err.Clear
			End If
		End With
	End If
End Function

'SUMMITCAPITALWEB_WebImage
Public Function SUMMITCAPITALWEB_WebImage(strTransName, objoBrowser, objPage, objWebImage)
	On Error Resume Next
	'Call FW_WriteLog("TransName",strTransName)
	With Browser(objoBrowser).Page(objPage)
		If .Image(objWebImage).Exist(60) Then
			.Image(objWebImage).WaitProperty "visible", True, 60000
			.Image(objWebImage).Click	 
			strSuccess = "พบข้อมูล " & objWebImage & " บนหน้าจอ"
			Call FW_PrintInfo("micPass", objWebImage, " Expected Result : " & strSuccess & " PASS")  
			strStatus = "Pass"
			'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebImage " & strStatus & " : " & strSuccess, "")
			Err.Clear					
		Else
			strBusinessError = "ไม่พบข้อมูล " & objWebImage & " บนหน้าจอ"
			strTechnicalError = Err.Description
			Call FW_PrintInfo("micFail", objWebImage, " Expected Result : " & strBusinessError & " : " & strTechnicalError & " FAIL")  
		    strStatus = "Fail"
		    strResultStatus = "Fail"
		    'Call FW_WriteLog("Capture SUMMITCAPITALWEB_WebImage " & strStatus & " : " & strBusinessError & " : " & strTechnicalError, "")
			Err.Clear
		End If
	End With
End Function

Public Function MaximizePageSUMMITCAPITAL(strWindow)
	On error resume next 
   If Window(strWindow).Exist(60) Then
     Window(strWindow).Maximize
   End If
End Function

Public Sub FW_OpenWebBrowserSUMMITCAPITAL(strTransName, objBrowser, objPage, strUrl) ' objWebElement, strWaitMessage
   	SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe", strUrl
End Sub

'Public Sub FW_OpenWebBrowserSUMMITCAPITAL(strTransName, objBrowser, objPage, strUrl, strBrowser) ' objWebElement, strWaitMessage
'	If UCase(strBrowser) = "IE" Then
'   		SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe", strUrl
'	ElseIf UCase(strBrowser) = "CHROME" Then
'		SystemUtil.Run "C:\Program Files\Google\Chrome\Application\chrome.exe", strUrl
'	ElseIf UCase(strBrowser) = "FIREFOX" Then
'		SystemUtil.Run "C:\Program Files (x86)\Mozilla Firefox\firefox.exe", strUrl
'	End If
'	
'	With Browser(objBrowser)
'		If .Page(objPage).Exist(60) Then
'				.Page(objPage).Sync
'		End If
'	End With
'End Sub

Public Function Ceil(p_Number)
    Ceil = 0 - INT( 0 - p_Number)
End Function
