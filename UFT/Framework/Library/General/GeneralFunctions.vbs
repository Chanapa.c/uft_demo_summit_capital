'********** Open Web Browser IE, Chrome, FireFox **********
Public Sub FW_OpenWebBrowser(strTransName, objBrowser, objPage, objWebElement, strWaitMessage, strUrl, strBrowser)
	'Call FW_TransactionStart(strTransName)
	If UCase(strBrowser) = "IE" Then
   		SystemUtil.Run "C:\Program Files\Internet Explorer\iexplore.exe", strUrl, "", ""
	ElseIf UCase(strBrowser) = "CHROME" Then
		SystemUtil.Run "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", strUrl, "", "", 3
	ElseIf UCase(strBrowser) = "FIREFOX" Then
		SystemUtil.Run "C:\Program Files (x86)\Mozilla Firefox\firefox.exe", strUrl, "", ""
	End If
	If Browser(objBrowser).Page(objPage).Exist(60) Then
		With Browser(objBrowser).Page(objPage)
			Browser(objBrowser).Page(objPage).Sync
			If .WebElement(objWebElement).Exist(60) Then
				.WebElement(objWebElement).WaitProperty "innerhtml", strWaitMessage, 60000
			End If
		End With
	End If

'	Call FW_CaptureScreenShot("", strTransName)
'	Call SaveStatus("Pass", myFile, "", "")
'	Call FW_TransactionEnd(strTransName)
End Sub
'**********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserChrome(transName)
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'chrome.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call SaveStatus("Pass", "", "", "")
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserChromeWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'chrome.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call SaveStatus("Pass", "", "")
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserFireFox(transName)
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'firefox.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserFireFoxWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'firefox.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserIE(transName)
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'iexplore.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Web Browser IE, Chrome, FireFox **********
Public Sub FW_CloseWebBrowserIEWhenError()
	On Error Resume Next    
	'Call FW_TransactionStart(transName)
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where Name = 'iexplore.exe'")
	For Each objProcess In colProcessList
		objProcess.Terminate()
	Next
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************************************

'********** Close Current **********
Public Sub FW_CloseCurrentBrowser(transName)
	On error resume next    
	'Call FW_TransactionStart(transName)
	If Browser(obj_Browser).Exist(WAIT_SHORT) Then
		Browser(obj_Browser).Close
	Else
		FW_PrintInfo "micFail", obj_Browser, obj_Browser & " Browser Cannot Close Error Description : " & Err.Description
		Err.Clear                                                                                               
	End If
	'Call FW_TransactionEnd(transName)
End Sub
'***********************************

'********* Compare Text Result *********
Public Function CompareTextResult(strExpected, strActual, strStepName, strDetail)
	On Error Resume Next    
	If strExpected = strActual Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		'Print "- Compare Text Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compare Text Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus = "Pass"
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		'Print "- Compare Text Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compare Text Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetails & vbNewLine	
		strResultStatus = "Fail"
		FW_CaptureScreenShot("")
	End If
	 
End Function
'***************************************

'********* Report Result *********
Public Function ReportResult(strStatus,strStepName,strDetail)
	If LCase(strStatus) = "micpass" or LCase(strStatus) = "pass" or LCase(strStatus) = "exist" or LCase(strStatus) = "true" or LCase(strStatus) = "matching" Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		'print "- Report Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Report Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus="Pass"	
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		'print "- Report Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Report Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine	
		strResultStatus = "Fail"	
		FW_CaptureScreenShot("")	'Call Function SnapShot Imag png
	End If
End Function
'*********************************

'********* PrintInfo *********
Public Function FW_PrintInfo(intStatus, strSource, strDetail)
	If intStatus = "micPass" Then
		Reporter.ReportEvent micPass, strSource, strDetail
	Else
		Reporter.ReportEvent micFail, strSource, strDetail	                              			
	End If
	Print intStatus & " " & strSource & " " & strDetail
End Function
'*****************************

'********* Create Folder *********
Public Function FW_CreateFolder(strFolderPath)
	'"C:\SampleFolder"
	Set obj = Createobject("Scripting.FileSystemObject")
	
	If obj.FolderExists(strFolderPath) = False Then
		tmpArr = Split(strFolderPath, "\")
		tmpPath = tmpArr(0)
		For i = 1 To UBound(tmpArr)
		    If Not obj.FolderExists(tmpPath) Then
		      obj.CreateFolder tmpPath
		    End If
	    	tmpPath = tmpPath & "\" & tmpArr(i)
		Next
	
		'obj.CreateFolder strFolderPath
		'obj.CreateFolder StrTemp
	End If
	'Clear Object
	Set obj = Nothing
End Function
'*********************************

'********* Create Sub Folder *********
Public Function CreateSubFolder(strSubFolderName)
	strSubFolderPath = strSubFolderName		'"C:\SampleFolder"
	Set obj = Createobject("Scripting.FileSystemObject")
	If obj.FolderExists(strSubFolderPath) = False Then
	 obj.CreateFolder strSubFolderPath
	End If
	Set obj = Nothing
End Function
'*************************************

'********* Show File List in Folder *********
Function ShowFileList(folderspec)
	Dim fso, f, f1, fc, s
   	Set fso = CreateObject("Scripting.FileSystemObject")
   	Set f = fso.GetFolder(folderspec)
   	Set fc = f.Files
   	For Each f1 in fc
		s = s & f1.name 
		s = s &   "<BR>"
   	Next
   	ShowFileList = s
End Function
'********************************************

'********* Report File Status *********
Function ReportFileStatus(filespec)
	Dim fso, msg
   	Set fso = CreateObject("Scripting.FileSystemObject")
   	If (fso.FileExists(filespec)) Then
    	msg = filespec & " exists."
   	Else
   		msg = filespec & " doesn't exist."
  	End If
   	ReportFileStatus = msg
End Function
'**************************************

'********* Show Folder Size *********
Function ShowFolderSize(filespec)
   Dim fso, f, s
   Set fso = CreateObject("Scripting.FileSystemObject")
   Set f = fso.GetFolder(filespec)
   's = UCase(f.Name) & " uses " & f.size & " bytes."
   'ShowFolderSize = s
   ShowFolderSize = f.size
End Function
'************************************

'********* Delete File *********
Function FW_DeleteFile(sourceFile)
	Set fso = CreateObject("Scripting.FileSystemObject")
	'Print "DeleteFile : " & sourceFile
	'SourceFile="D:\Automate\PromptPay\Result\20170908_101715\20170908_101715_Result.xlsx"
   	If (fso.FileExists(sourceFile)) Then
      	'File to be  deleted.  Sourcefile="C:\copy.txt"  'Delete the file
      	fso.DeleteFile sourceFile
      	'print "Source File Delete : "&SourceFile
   	Else
      	'Print "Source File doesn't exist : " & sourceFile
   	End If
	Set fso = Nothing
End Function
'*******************************

'********* Delete Folder *********
Function DeleteFolder(strFolder)
	Dim objFS
	'Create a file system object
	Set objFS = CreateObject("Scripting.FileSystemObject")
	'Check that the source folder exists
	If Not objFS.FolderExists(strFolder) Then
		'fail if the source does not exist
	    'Print "Unable to Delete Folder '" & strFolder & "', It Does Not Exist"
	Else
	    'Delete the folder
	    objFS.DeleteFolder strFolder
	End If
	' destroy the object
	Set objFS = Nothing
End Function
'*********************************

'********* Rename Folder *********
Function FW_RenameFolder(strSourceFolder, strDestinationFolder)
	Dim objFS
	'Create a file system object
	Set objFS = CreateObject("Scripting.FileSystemObject")
	objFS.MoveFolder strSourceFolder, strDestinationFolder
	Print "Rename Result Folder " & strSourceFolder & " --> " & strDestinationFolder
	Set objFS = Nothing
End Function
'*********************************

'********* Get Current Date Time Format [YYYYMMDD_HHMMSS] *********
Public Function GetCurrentDateTimeFormat
	GetCurrentDateTimeFormat = 0  
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now) 
	End If
	
	If Len(Day(Now)) = 1 Then 
		currentDay = 0 & Day(Now) 
	Else 
		currentDay = Day(Now) 
	End If
	
	If Len(Hour(Now)) = 1 Then 
		currentHour = 0 & Hour(Now) 
	Else 
		currentHour = Hour(Now) 
	End If
	
	If Len(Minute(Now)) = 1 Then    
		currentMinute = 0 & Minute(Now)    
	Else    
		currentMinute = Minute(Now) 
	End If
	
	If Len(Second(Now)) = 1 Then    
		currentSecond = 0 & Second(Now)    
	Else    
		currentSecond = Second(Now) 
	End If
	
	GetCurrentDateTimeFormat = Year(Now) & currentMonth & currentDay & "_" & currentHour & currentMinute & currentSecond  

End Function
'******************************************************************

'********* Capture Screen Shot *********
Public Function FW_CaptureScreenShot(strFileName)
	If LCase(CAPTURE_SCREEN_SHOT_STATUS) = "true" or LCase(CAPTURE_SCREEN_SHOT_STATUS) = "capture" Then
		If strFileName = "" or LCase(strFileName) = "default" Then		'Set "" Auto Define Folder and filename SnapShots
			myFile = strResultPathExecution & testCase & "_" & GetCurrentDateTimeFormat & ".png"
			
			'Myfile= strResultPathTestCase&"\"&strResultStatus&"_"&GetFormatedCurrentDate&".png" 
'			iRowCount = Datatable.getSheet("CapScreen").getRowCount
' 			Datatable.getSheet("CapScreen").setCurrentRow(iRowCount+1)
' 			DataTable.Value("No","CapScreen") = Myfile
		Else
			myFile = strFileName
		End If
	    Desktop.CaptureBitmap myFile, True
	End If
End Function
'***************************************

'********* Add Zero Folder Row [0000 - 9999] *********
Public Function FW_AddZeroFolderRow(row)
	FOLDER_ROW = ""
	If Len(row) = 1 Then
		FOLDER_ROW = "000" & row
	ElseIf Len(row) = 2 Then
		FOLDER_ROW = "00" & row
	ElseIf Len(row) = 3 Then
		FOLDER_ROW = "0" & row
	Else
		FOLDER_ROW = row
	End If
End Function
'*****************************************************

'********* Add Zero Folder Row [0000 - 9999] *********
Public Function FW_AddZeroRunningCapture(runningCapture)
	RUNNING_CAPTURE_PAGE = ""
	If Len(runningCapture) = 1 Then
		RUNNING_CAPTURE_PAGE = "000" & runningCapture
	ElseIf Len(runningCapture) = 2 Then
		RUNNING_CAPTURE_PAGE = "00" & runningCapture
	ElseIf Len(runningCapture) = 3 Then
		RUNNING_CAPTURE_PAGE = "0" & runningCapture
	Else
		RUNNING_CAPTURE_PAGE = runningCapture
	End If
End Function
'*****************************************************

'********* Capture Screen Shot *********
Public Function FW_CaptureScreenShot(transName, strSheetName, currentRow)
	'Print "CAPTURE_SCREEN_SHOT_STATUS --> " & CAPTURE_SCREEN_SHOT_STATUS
	If CAPTURE_SCREEN_SHOT_STATUS = "Y" Then
		'Print "RUNNING_CAPTURE --> " & RUNNING_CAPTURE
		FW_AddZeroRunningCapture(RUNNING_CAPTURE)
		'Print "RUNNING_CAPTURE_PAGE --> " & RUNNING_CAPTURE_PAGE
		myRootPath = strResultPathFolderRowExecution
		'Print "myRootPath --> " & myRootPath
		myFile = strResultPathFolderRowExecution & RUNNING_CAPTURE_PAGE & "_" & transName & "_" & GetCurrentDateTimeFormat & ".png"
		If RUNNING_CAPTURE = 1 Then
			Datatable.getSheet(strSheetName).setCurrentRow(currentRow)
			DataTable.Value("CAPTURE_SCREEN_SHOT", strSheetName) = "'" & Trim(myRootPath)
		End If
		
		RUNNING_CAPTURE = RUNNING_CAPTURE + 1
		'Print "Capture MyFile --> " & myFile
		Wait 1.5
	    Desktop.CaptureBitmap myFile, True
	End If
End Function
'***************************************

'********* Create Initial FrameWork *********
Public Function FW_CreateInitialFramework()
	strImportFileTestData = PATH_IMPORT_DATA & "\" & FILE_TEST_DATA
	
	'Start Update Date Version and Execute Version
	executeVersion = 0
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False	
	objExcel.Application.WindowState = -4140
	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets("SummaryReport")
	dateVersion = objWorkSheet.Cells(10,2).value
	executeVersion = objWorkSheet.Cells(11,2).value
	
	If Len(Day(Now)) = 1 Then  
		currentDay = 0 & Day(Now)   
	Else  
		currentDay = Day(Now) 
	End If
	
	currentMonth = ""
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now)
	End If
		
	currentDate = Year(Now) & "" & currentMonth & "" & currentDay
	'Print "DateVersion --> " & dateVersion
	'Print "ExecuteVersion --> " & executeVersion
	'Print "CurrentDate --> " & currentDate
	
	If Trim(dateVersion) = Trim(currentDate) Then
		objWorkSheet.Cells(11,2).value = executeVersion + 1
		executeVersion = objWorkSheet.Cells(11,2).value
		'Print "ExecuteVersion Add --> " & executeVersion
	Else
		objWorkSheet.Cells(10,2).value = currentDate
		objWorkSheet.Cells(11,2).value = 1
		dateVersion = objWorkSheet.Cells(10,2).value
		executeVersion = objWorkSheet.Cells(11,2).value
		'Print "DateVersion Clear --> " & dateVersion
		'Print "ExecuteVersion Clear --> " & executeVersion
	End If
	
	EXECUTE_VERSION = executeVersion
	
	objWorkBook.Save()
	objWorkBook.Close
    objExcel.DisplayAlerts = False
	objExcel.Quit

	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing

	'Import Test Data
	Datatable.Import strImportFileTestData
	EXECUTE_STATUS = "PASS"
	'TestStep
	Datatable.AddSheet("Log")
	DataTable.GetSheet("Log").AddParameter "Log", ""

	'TestStep
'	Datatable.AddSheet("TestStep")
'	DataTable.GetSheet("TestStep").AddParameter "LogStep", ""
'	DataTable.GetSheet("TestStep").AddParameter "LogStatus", ""
'	DataTable.GetSheet("TestStep").AddParameter "CaptureScreenShot", ""
'	DataTable.GetSheet("TestStep").AddParameter "StartTime", ""
'	DataTable.GetSheet("TestStep").AddParameter "EndTime", ""
	
	'LogTransactionTime
	Datatable.AddSheet("TestStep")
	DataTable.GetSheet("TestStep").AddParameter "FunctionNo" ,""
	DataTable.GetSheet("TestStep").AddParameter "StepName" ,""
	DataTable.GetSheet("TestStep").AddParameter "Status", ""
	DataTable.GetSheet("TestStep").AddParameter "CaptureScreenShot", ""
	DataTable.GetSheet("TestStep").AddParameter "StartTime" ,""
	DataTable.GetSheet("TestStep").AddParameter "EndTime" ,""
	DataTable.GetSheet("TestStep").AddParameter "BusinessError" ,""
	DataTable.GetSheet("TestStep").AddParameter "TechnicalError" ,""
'	Datatable.AddSheet("CapScreen")
'	DataTable.GetSheet("CapScreen").AddParameter "No" ,""

	'SummaryReport
    Datatable.AddSheet("SummaryReport")
    Datatable.ImportSheet strImportFileTestData, "SummaryReport", "SummaryReport"
    Datatable.getSheet("SummaryReport").setCurrentRow(2)
    DataTable.Value("Result", "SummaryReport") = "'" & startExecuteDate
    Datatable.getSheet("SummaryReport").setCurrentRow(3)
    DataTable.Value("Result", "SummaryReport") = "'" & startExecuteTime
    
	strGetTime = GetCurrentDateTimeFormat
	strResultPathExecution = PATH_EXPORT_RESULT & strGetTime & "\"
	strResultTmpPathExecution = PATH_EXPORT_RESULT & strGetTime & "\" & "Tmp" & "\"  'subfolder name
	strTmpResultPathExecution = PATH_EXPORT_RESULT & strGetTime & "\" & "TMP"
	strResultFileExecution = strGetTime
	strResultFileName = strResultPathExecution & R_FILENAME_RESULT & strResultFileExecution & "_" & EXECUTE_VERSION
	'FileTMPNameResult = strResultTmpPathExcel & strResultFileExcel
	ResultTmpFileName = strResultTmpPathExecution & strResultFileExecution
	
	'Print strResultPathExecution
	'Print strResultFileName
	'Print strResultFileExecution
	
	Call FW_CreateFolder(strResultPathExecution)
	'Call CreateSubFolder(strTmpResultPathExecution)
	'Call FW_WriteLog("********************************************", "")
	'Call FW_WriteLog("Start Execution", "OK" & " --> " & now())
	'Call FW_WriteLog("Result Path Execution", "OK" & " --> " & strResultPathExecution)
	'Call FW_WriteLog("Result FileName", "OK" & " --> " & strResultFileName)
	'Call FW_WriteLog("********************************************", "")
End Function
'********************************************

'********* Get ScriptName and TestData from Configuration *********
Public Function FW_GetConfigData(strConfigData, strSheetName)
	strImportFileTestData = strConfigData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	strRowCount = objWorkSheet.UsedRange.Rows.Count
	strColumnCount = objWorkSheet.UsedRange.Columns.Count
	
	'Print "GetConfigData RowCount --> " & strRowCount
	'Print "GetConfigData ColumnCount --> " & strColumnCount
	
	Set dataColumn = CreateObject("Scripting.Dictionary")

	For StartRow = 1 To strRowCount Step 1
	'Print "GetConfigData Data Row --> " & StartRow
		key = objWorkSheet.Cells(StartRow, 1).Value 'UFT_F_MF_SIGNON
		value = objWorkSheet.Cells(StartRow, 2).Value 'C:\SIUpgrade\UFT\Framework\Data\Mainframe\MF_SIGNON.xlsx
		verify = objWorkSheet.Cells(StartRow, 3).Value 'N
		capture = objWorkSheet.Cells(StartRow, 4).Value 'Y
		path = objWorkSheet.Cells(StartRow, 5).Value 'C:\SIUpgrade\UFT\Framework\CaptureScreen
		dataColumn.Add Trim(key), Trim(value & "|" & verify & "|" & capture & "|" & path)
		'Print "Add Script Name --> " & key & ", Test Data --> " & value
		'dataColumn(objWorkSheet.Cells(StartRow, 1).Value) = data
		'Print "Get Value --> " & dataColumn(Trim(key))
	Next
	
	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Get Total Data from TestData *********
Public Function FW_GetTotalTestData(strTestData, strSheetName)
	strImportFileTestData = strTestData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	strRowCount = objWorkSheet.UsedRange.Rows.Count
	strColumnCount = objWorkSheet.UsedRange.Columns.Count
	
	'Print "GetTotalTestData RowCount --> " & strRowCount

	Set dataColumn = CreateObject("Scripting.Dictionary")
	dataColumn.Add "TotalTestData", strRowCount

	Set dataTag = CreateObject("Scripting.Dictionary")
	count = 1
	For StartRow = 2 To strRowCount Step 1
		'Print "GetTotalTestData Get Data Row --> " & count
		key = "TAG" & "_" & count
		value = objWorkSheet.Cells(StartRow, 1).Value
		dataTag.Add Trim(key), Trim(value)
		'Print "Add Script Name --> " & key & ", Test Data --> " & value
		'Print "Get " & key & " Value --> " & dataColumn(Trim(key))
		count = count + 1
	Next
	
	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Create Result in Data Table *********
Public Function FW_GetTestData(strTestData, strSheetName, currentRow)
	strImportFileTestData = strTestData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	
	wait 3
		
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	strRowCount = objWorkSheet.UsedRange.Rows.Count
	strColumnCount = 0
	For StartColumn = 1 To 256 Step 1
		label = objWorkSheet.Cells(1, StartColumn).Value
		If label = "" Then
			'Print "Column Header --> empty exit for column" & label
			Exit For
		End If
		strColumnCount = strColumnCount + 1
	Next

	'Print "GetTestData RowCount --> " & strRowCount
	'Print "GetTestData ColumnCount --> " & strColumnCount
	
	Set dataColumn = CreateObject("Scripting.Dictionary")
	
	'Flag = 0
	totalColumn = 0
	For StartRow = 1 To 2 Step 1
		For StartColumn = 1 To strColumnCount Step 1
			'Print "FW_GetTestData Get Data Column --> " & StartColumn
			If StartRow = 1 Then
				label = objWorkSheet.Cells(StartRow, StartColumn).Value
				If label = "" Then
					'Print "Column Header --> empty exit for column" & label
					Exit For
				End If
				'Print "Add Column Header --> " & label
				dataColumn.Add label, ""
			Else 
				data = objWorkSheet.Cells(currentRow+1, StartColumn).Value
'				If data = "" Then
'					Print "Test Value --> empty exit for value"
'					flag = 1
'					Exit For
'				End If
				'Print "Add Test Value --> " & data
				dataColumn(objWorkSheet.Cells(1, StartColumn).Value) = data
			End If
		Next
'		If flag = 1 Then
'			Exit For
'		End If
	Next

	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Create Result in Data Table *********
Public Function FW_GetResultData(strTestData, strSheetName, currentRow)
	strImportFileTestData = strTestData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	strRowCount = objWorkSheet.UsedRange.Rows.Count
	strColumnCount = objWorkSheet.UsedRange.Columns.Count
	
	'Print "strRowCount --> " & strRowCount
	'Print "strColumnCount --> " & strColumnCount
	
	Set dataColumn = CreateObject("Scripting.Dictionary")
	
	totalColumn = 0
	For StartRow = 1 To 1 Step 1
		For StartColumn = 1 To strColumnCount Step 1
			'Print "Get Data Round --> " & StartColumn
			label = objWorkSheet.Cells(StartRow, StartColumn).Value
			If label = "" Then
				'Print "Column Header --> empty exit for column" & label
				Exit For
			End If
			'Print "Add Column Header --> " & label
			dataColumn.Add label, ""
			totalColumn = totalColumn + 1
		Next
	Next
	
	For StartColumn = 1 To totalColumn Step 1
		'Print "Get Data Round --> " & StartColumn
		data = objWorkSheet.Cells(currentRow + 1, StartColumn).Value
		'Print "Add Test Value --> " & data
		dataColumn(objWorkSheet.Cells(1, StartColumn).Value) = data
	Next
	
	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Get Total Data from TestData *********
Public Function FW_SaveCitizenIDForLoan(strTestData, strSheetName, idtype, citizenid, title, firstname, lastname, currentRow)
	strImportFileTestData = strTestData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	'Print "currentRow --> " & currentRow
	'Print "Add IdType --> " & idtype
	'Print "Add Citizen ID --> " & citizenid
	'Print "Add Title --> " & title
	'Print "Add Firstname --> " & firstname
	'Print "Add Lastname --> " & lastname
	
	objWorkSheet.Cells(currentRow,9).value = idtype
	objWorkSheet.Cells(currentRow,10).value = citizenid
	objWorkSheet.Cells(currentRow,11).value = title
	objWorkSheet.Cells(currentRow,12).value = firstname
	objWorkSheet.Cells(currentRow,13).value = lastname
				
	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Get Total Data from TestData *********
Public Function FW_SaveCitizenIDForTransfer(strTestData, strSheetName, idtype, citizenid, title, firstname, lastname, currentRow)
	strImportFileTestData = strTestData
	
	'Start Update Date Version and Execute Version
	Set objExcel = CreateObject("Excel.Application")
	objExcel.visible = False
	objExcel.Application.WindowState = -4140

	Set objWorkBook = objExcel.Workbooks.Open(strImportFileTestData)
	Set objWorkSheet = objWorkBook.WorkSheets(strSheetName)
	
	'Print "currentRow --> " & currentRow
	'Print "Add IdType --> " & idtype
	'Print "Add Citizen ID --> " & citizenid
	'Print "Add Title --> " & title
	'Print "Add Firstname --> " & firstname
	'Print "Add Lastname --> " & lastname
	
	objWorkSheet.Cells(currentRow,9).value = idtype
	objWorkSheet.Cells(currentRow,10).value = citizenid
	objWorkSheet.Cells(currentRow,11).value = title
	objWorkSheet.Cells(currentRow,12).value = firstname
	objWorkSheet.Cells(currentRow,13).value = lastname
				
	objWorkBook.Save()
	objWorkBook.Close
	objExcel.DisplayAlerts = False
	objExcel.Quit
	
	Set objWorkSheet = Nothing
	Set objWorkBook = Nothing
	Set objExcel = nothing
	
End Function
'********************************************

'********* Create Result in Data Table *********
Public Function FW_CreateResult(dtRow)
	If Trim(strResultStatus) = "" or Trim(LCase(strResultStatus)) = "n/a" Then
		strResultStatus = "Pass"
	Else
		strResultStatus = "Fail"
	End If

	DataTable.GetSheet(SHEET_TEST_CONTROLLER).SetCurrentRow(dtRow)
	DataTable(RESULT_STATUS, SHEET_TEST_CONTROLLER) = strResultStatus
	DataTable(RESULT_DESCRIPTION, SHEET_TEST_CONTROLLER) = strResultDescription
	'Call FW_WriteLog("Result Status", strResultStatus)
	'Call FW_WriteLog("Result Description", strResultDescription)
	'Call FW_WriteLog("End Execution", Now())
	'Call FW_WriteLog("********************************************", "")
	strResultDescription = "'"
	strResultStatus = "N/A"
End Function
'***********************************************

'TMP Excel
Public Function FW_Result_Export_Tmp()
	Dim strRowCount
	Datatable.AddSheet("SummaryReport")
	Datatable.getSheet("SummaryReport").setCurrentRow(5)
    DataTable.Value("Result","SummaryReport") = StoptExecuteTime  
    Call SummaryAllTest
   	TmpExecuteDate = GetFormatedCurrentDate()
	OriginalFileExport = FileTMPNameResult&"TMP_Result"&".xlsx"
	'OriginalFileExport = FileTMPNameResult&"TMP_Result"&".xlsx"
	Datatable.ExportSheet OriginalFileExport,"SummaryReport"
	Datatable.ExportSheet OriginalFileExport,"Global"
	Datatable.ExportSheet OriginalFileExport,"Main"
	Datatable.ExportSheet OriginalFileExport,"TestStep"
	Datatable.ExportSheet OriginalFileExport,"LogTimeTransaction"
	Set fso = CreateObject("Excel.Application")
	Set objExcel = fso   	    
	Set objWorkBook = objExcel.Workbooks.Open (OriginalFileExport)
	Set objWorkSheet = objWorkBook.WorkSheets("Global")
	Set objWorkSheetLog = objWorkBook.WorkSheets("Main")
	'	Extend Width in column
	objWorkSheet.Columns("E:E").ColumnWidth = 100
	objWorkSheetLog.Columns("A:A").ColumnWidth = 100
	'	Set left-align the text.
	objWorkSheet.Columns("E:E").HorizontalAlignment = -4131
	'strColumnCount = 30
	'   Get Row Count Range From Range
    strRowCount = objWorkSheet.UsedRange.Rows.Count 
    For StartRow = 1 To strRowCount
        strTestStatus = Trim(LCase(objWorkSheet.Cells(StartRow, "D").Value))
        If StartRow = 1 Then
        	strColorIndexValue = 17 'Color Green Header
        Else 
	        Select Case strTestStatus
	            Case "pass"
	                strColorIndexValue = 43  'Color Green
	            Case "fail"
	                strColorIndexValue = 3  'Color Red
	            Case "no run"
	                strColorIndexValue = 6 'Color Yellow
	            Case Else
	                strColorIndexValue = 2  'Color White
	               	'Print "Test Status Not Match Row " & StartRow & " at : " & OriginalFileExport
	        End Select
        End If
        
        strRange = ""
        If StartRow = 1 Then
        	strRange = "A" & StartRow & ":E" & StartRow
        Else
            strRange = "D" & StartRow & ":D" & StartRow
        End If
        'Change Cell Color follow Test Status 
        objWorkSheet.Range(strRange).Interior.ColorIndex = strColorIndexValue
        
'        For StartColumn = 6 To strColumnCount
'        	strData = "F" & strColumnCount & ":V" & strColumnCount
'		    objWorkSheet.Range(strData).EntireColumn.Delete
'        Next
		
    Next
	
	objWorkbook.SaveAs(FileTMPNameResult&"_"&TmpExecuteDate&"_TMP.xlsx")
'    objworkbook.Saved=True
	'Close workbook
	objworkbook.Close		'commented out to leave workbook open for review
    objExcel.DisplayAlerts = False
	objExcel.Quit    

	Set objWorkSheet = Nothing
	Set objWorkbook = Nothing	
	Set objExcel = nothing
	
	'	Delete Original File Export from DataTable
	Call FileDelete(OriginalFileExport)
End Function

'********* Export Result and Custom Result *********
Public Function FW_ExportResult()
	Dim strRowCount
	'Datatable.AddSheet("Summary")
	Datatable.getSheet("SummaryReport").setCurrentRow(4)
    DataTable.Value("Result", "SummaryReport") = "'" & stopExecuteTime  
    Call FW_SummaryAllTest()
    
	originalFileExport = strResultFileName & ".xlsx"
	'Datatable.ExportSheet originalFileExport, "Global"
	Datatable.ExportSheet originalFileExport, SHEET_TEST_CONTROLLER
	Datatable.ExportSheet originalFileExport, "SummaryReport"
    Datatable.ExportSheet originalFileExport, "TestStep"
	'Datatable.ExportSheet originalFileExport, "LogTransactionTime"
	'Datatable.ExportSheet originalFileExport, "Log"
	
	Set fso = CreateObject("Excel.Application")
	Set objExcel = fso   	    
	Set objWorkBook = objExcel.Workbooks.Open(originalFileExport)
	'Set objWorkSheet = objWorkBook.WorkSheets("Global")
	
	Set objWorkSheetTestController = objWorkBook.WorkSheets(SHEET_TEST_CONTROLLER)
	'Set objWorkSheetLogTransactionTime = objWorkBook.WorkSheets("LogTransactionTime")
	Set objWorkSheetSummaryReport = objWorkBook.WorkSheets("SummaryReport")
	Set objWorkSheetTestStep = objWorkBook.WorkSheets("TestStep")
	'Set objWorkSheetLog = objWorkBook.WorkSheets("Log")
	
	'Extend Width in column
	objWorkSheetTestController.Columns("A:A").ColumnWidth = 10
	objWorkSheetTestController.Columns("B:B").ColumnWidth = 40
	objWorkSheetTestController.Columns("C:C").ColumnWidth = 40
	objWorkSheetTestController.Columns("D:D").ColumnWidth = 40
	objWorkSheetTestController.Columns("E:E").ColumnWidth = 40
	objWorkSheetTestController.Columns("F:F").ColumnWidth = 40
	objWorkSheetTestController.Columns("G:G").ColumnWidth = 10
	objWorkSheetTestController.Columns("H:H").ColumnWidth = 10
	objWorkSheetTestController.Columns("I:I").ColumnWidth = 40
	'Set left-align the text.
	'objWorkSheetTestController.Columns("G:G").HorizontalAlignment = -4131
	objWorkSheetTestController.Range("A:I").Font.Name = "Arial"
	
'	'TestStep
'	'Extend Width in column
'	objWorkSheetTestStep.Columns("A:A").ColumnWidth = 40
'	objWorkSheetTestStep.Columns("B:B").ColumnWidth = 15
'	objWorkSheetTestStep.Columns("C:C").ColumnWidth = 80
'	objWorkSheetTestStep.Columns("D:D").ColumnWidth = 15
'	objWorkSheetTestStep.Columns("E:E").ColumnWidth = 15
'	'Change Cell Color Log
'	objWorkSheetTestStep.Range("A:E").Font.Name = "Arial"
'    objWorkSheetTestStep.Range("A1:E1").Interior.ColorIndex = 27
'    objWorkSheetTestStep.Range("A1:E1").Font.ColorIndex = 1
'    'Change Cell Font Bold Log
'    objWorkSheetTestStep.Range("A1:E1").Font.Bold = True
	
	'TestStep
	'Extend Width in column
	objWorkSheetTestStep.Columns("A:A").ColumnWidth = 15
	objWorkSheetTestStep.Columns("B:B").ColumnWidth = 40
	objWorkSheetTestStep.Columns("C:C").ColumnWidth = 15
	objWorkSheetTestStep.Columns("D:D").ColumnWidth = 40
	objWorkSheetTestStep.Columns("E:F").ColumnWidth = 15
	objWorkSheetTestStep.Columns("G:H").ColumnWidth = 40
	'Change Cell Color Log
	objWorkSheetTestStep.Range("A:H").Font.Name = "Arial"
    objWorkSheetTestStep.Range("A1:H1").Interior.ColorIndex = 27
    objWorkSheetTestStep.Range("A1:H1").Font.ColorIndex = 1
    'Change Cell Font Bold Log
    objWorkSheetTestStep.Range("A1:H1").Font.Bold = True
	
	'SummaryReport
	'Extend Width in column
	objWorkSheetSummaryReport.Columns("A:A").ColumnWidth = 35
	objWorkSheetSummaryReport.Columns("B:B").ColumnWidth = 10
	'Change Cell Color Log
	objWorkSheetSummaryReport.Range("A:B").Font.Name = "Arial"
    objWorkSheetSummaryReport.Range("A1:B1").Interior.ColorIndex = 27
    objWorkSheetSummaryReport.Range("A1:B1").Font.ColorIndex = 1
    'Change Cell Font Bold Log
    objWorkSheetSummaryReport.Range("A1:B1").Font.Bold = True
	
	'Log
	'Extend Width in column
'	objWorkSheetLog.Columns("A:A").ColumnWidth = 100
'	objWorkSheetLog.Range("A:A").Font.Name = "Arial"
'	'Change Cell Color Log
'    objWorkSheetLog.Range("A1:A1").Interior.ColorIndex = 27
'    objWorkSheetLog.Range("A1:A1").Font.ColorIndex = 1
'    'Change Cell Font Bold Log
'    objWorkSheetLog.Range("A1:A1").Font.Bold = True
    
	'strColumnCount = 30
	'Get Row Count Range From Range
    strRowCount = objWorkSheetTestController.UsedRange.Rows.Count 
    For startRow = 1 To strRowCount
        strTestStatus = Trim(LCase(objWorkSheetTestController.Cells(startRow, "F").Value))
        If startRow = 1 Then
        	strColorIndexValue = 27 'Color Light Yellow Header
        Else 
	        Select Case strTestStatus
	            Case "pass"
	                strColorIndexValue = 4  'Color Light Green
	            Case "fail"
	                strColorIndexValue = 3  'Color Light Red
	            Case "no run"
	                strColorIndexValue = 48 'Color Yellow
	            Case Else
	                strColorIndexValue = 2  'Color White
	               	'Print "Test Status Not Match Row " & StartRow & " at : " & OriginalFileExport
	        End Select
        End If
        
        strRange = ""
        If startRow = 1 Then
        	strRange = "A" & startRow & ":I" & startRow
        Else
            strRange = "G" & startRow & ":G" & startRow
        End If
        'Change Cell Color follow Test Status 
        objWorkSheetTestController.Range(strRange).Interior.ColorIndex = strColorIndexValue
        objWorkSheetTestController.Range(strRange).Font.ColorIndex = 1
        
        'Change Cell Font Bold 
        objWorkSheetTestController.Range(strRange).Font.Bold = True
        
'        For StartColumn = 6 To strColumnCount
'        	strData = "F" & strColumnCount & ":V" & strColumnCount
'		    objWorkSheet.Range(strData).EntireColumn.Delete
'        Next
		
    Next
	
	objWorkSheetTestController.SaveAs(strResultFileName & "_" & EXECUTE_STATUS & ".xlsx")
	'objworkbook.Saved=True
	'Close Workbook
	objWorkbook.Close		'Commented out to leave workbook open for review
    objExcel.DisplayAlerts = False
	objExcel.Quit    

	Set objWorkSheetSummaryReport = Nothing
	'Set objWorkSheetLogTransactionTime = Nothing
    Set objWorkSheetTestStep = Nothing
	'Set objWorkSheetLog = Nothing
	Set objWorkSheetTestController = Nothing
	Set objWorkbook = Nothing	
	Set objExcel = Nothing
	
	'Delete Original File Export from DataTable
	Call FW_DeleteFile(originalFileExport)
	
	'Rename Result Folder
	sourceFolder = PATH_EXPORT_RESULT & strResultFileExecution
	destinationFolder = PATH_EXPORT_RESULT & R_FILENAME_RESULT & strResultFileExecution & "_" & EXECUTE_VERSION & "_" & EXECUTE_STATUS 
	Print "SourceFolder --> " & sourceFolder
	Print "DestinationFolder --> " & destinationFolder 
	Call FW_RenameFolder(sourceFolder, destinationFolder)
End Function
'***************************************************

'********** Exit Iteration **********
Public Function FW_ExitIteration(strDescription)
	strResultStatus = "Fail"
	strResultDescription = strDescription
	ExitActionIteration
End Function
'************************************ 

'********** Write Log **********
Public Function FW_WriteLog(ByVal sAction, ByVal sValue)
	If sValue = "" Then
		Print sAction
		runLog = runLog & sAction
	Else
		sAction = sAction & " = " & sValue
		Print sAction
		runLog = runLog & sAction
	End If
	runLog = runLog & chr(10)
	iRowCount = Datatable.getSheet("Global").getRowCount
 	Datatable.getSheet("Global").setCurrentRow(iRowCount+1)
	DataTable("Log", "Global") = runLog
End Function
'******************************* 

'********** Start Execute **********
Public Function FW_StartExecute(strSheetName, currentRow)
	currentDay = ""
	If Len(Day(Now)) = 1 Then  
		currentDay = 0 & Day(Now)   
	Else  
		currentDay = Day(Now) 
	End If
	
	currentMonth = ""
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now) 
	End If
	
	currentHour = ""
	If Len(Hour(Now)) = 1 Then  
		currentHour = 0 & Hour(Now)   
	Else  
		currentHour = Hour(Now) 
	End If

	currentMinute = ""
	If Len(Minute(Now)) = 1 Then  
		currentMinute = 0 & Minute(Now)   
	Else  
		currentMinute = Minute(Now) 
	End If
	
	currentSecond = ""
	If Len(Second(Now)) = 1 Then  
		currentSecond = 0 & Second(Now)   
	Else  
		currentSecond = Second(Now) 
	End If
	
	startExecuteDate = currentDay & "/" & currentMonth & "/" & Year(Now)
	startExecuteTime = currentHour & ":" & currentMinute & ":" & currentSecond
	Datatable.getSheet(strSheetName).setCurrentRow(currentRow)
	DataTable.Value("TS_START_TIME", strSheetName) = "'" & Trim(startExecuteDate & " " & startExecuteTime)
'	Print "StartExecute --> " & startExecuteDate & " " & startExecuteTime
End Function
'***********************************

'********** Stop Execute **********
Public Function FW_StopExecute(strSheetName, currentRow)
	currentHour = ""
	If Len(Hour(Now)) = 1 Then  
		currentHour = 0 & Hour(Now)   
	Else  
		currentHour = Hour(Now) 
	End If

	currentMinute = ""
	If Len(Minute(Now)) = 1 Then  
		currentMinute = 0 & Minute(Now)   
	Else  
		currentMinute = Minute(Now) 
	End If
	
	currentSecond = ""
	If Len(Second(Now)) = 1 Then  
		currentSecond = 0 & Second(Now)   
	Else  
		currentSecond = Second(Now) 
	End If
	
	stopExecuteTime = currentHour & ":" & currentMinute & ":" & currentSecond
	Datatable.getSheet(strSheetName).setCurrentRow(currentRow)
	DataTable.Value("TS_END_TIME", strSheetName) = "'" & Trim(startExecuteDate & " " & stopExecuteTime)
	'Print "StopExecute --> " & stopExecuteTime
End Function
'**********************************

'********** Start Execute **********
Public Function FW_DurationTime()
	durationTime = ""
	'startExecuteTime
	'stopExecuteTime
	strDuration = 0
	strDuration = DateDiff("s",startExecuteTime, stopExecuteTime)
	Call FW_WriteLog("DurationTime(s) : ", strDuration)
	
	strHour = Round(strDuration / 3600,0)
	If Len(strHour) = 1 Then
		strHour = 0 & strHour 
	Else  
		strHour = strHour 
	End If
	Call FW_WriteLog("Hour(HH) : ", strHour)
	
	strMinute = Round(strDuration / 60,0)
	If Len(strMinute) = 1 Then
		strMinute = 0 & strMinute 
	Else  
		strMinute = strMinute 
	End If
	Call FW_WriteLog("Minute(MM) : ", strMinute)
	
	strSecond = strDuration mod 60
	If Len(strSecond) = 1 Then
		strSecond = 0 & strSecond 
	Else  
		strSecond = strSecond 
	End If
	Call FW_WriteLog("Second(SS) : ", strSecond)
	
	durationTime = strHour & ":" & strMinute & ":" & strSecond
	Call FW_WriteLog("DurationTime(HH:MM:SS) : ", durationTime)
	
End Function

'********** Summary All Test **********
Public Function FW_SummaryAllTest()
	strPass = 0
	strFail = 0
	For dtRow = 1 To DataTable.GetSheet(SHEET_TEST_CONTROLLER).GetRowCount Step 1
	DataTable.GetSheet(SHEET_TEST_CONTROLLER).SetCurrentRow(dtRow)
	strStatus = DataTable.Value("Status", SHEET_TEST_CONTROLLER)
		If strStatus ="Pass" Then
			strPass = strPass + 1
		ElseIf strStatus = "Fail" Then
			strFail = strFail + 1
		End If
	Next
	
	Call FW_DurationTime()
	
	Datatable.getSheet("SummaryReport").setCurrentRow(5)
	DataTable.Value("Result", "SummaryReport") =  durationTime
	
	Datatable.getSheet("SummaryReport").setCurrentRow(6)
	DataTable.Value("Result", "SummaryReport") =  strPass + strFail
	Datatable.getSheet("SummaryReport").setCurrentRow(7)
	DataTable.Value("Result", "SummaryReport") =  strPass
	
	strColorIndexValue = 43 
	Datatable.getSheet("SummaryReport").setCurrentRow(8)
    DataTable.Value("Result", "SummaryReport") = strFail
    
End Function
'**************************************

'********** Scroll Web Page **********
Public Function ScrollWebPage()
	Set objShell=CreateObject("WScript.Shell")
'	objShell.SendKeys "{PGUP}"
'	objShell.SendKeys "{PGDN}"
'	objShell.SendKeys "{END}"
End Function
'*************************************

'********** SendKey **********
Public Function Sendkey(strkey)
	Dim objShell
	Set objShell=CreateObject("WScript.Shell")
	objShell.SendKeys strkey
	Set objShell = Nothing
'	objShell.SendKeys "{PGUP}"
'	objShell.SendKeys "{PGDN}"
'	objShell.SendKeys "{END}"
End Function
'*****************************

'********** Compore Number Result **********
Public Function CompareNumberResult(strExpected, strActual, strStepName, strDetail)
	On Error Resume Next    
	If CDBL(strExpected) = CDBL(strActual) Then
		Reporter.ReportEvent micPass, strStepName, strDetail
		print "- Compore Number Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail
		strResultDescription = strResultDescription & "- Compore Number Result Pass | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine
		strResultStatus="Pass"
		FW_CaptureScreenShot("")
	Else
		Reporter.ReportEvent micFail, strStepName, strDetail
		print "- Compore Number Result Fail | StepName : " & strStepName & " | Step Detail : " & strDetail	
		strResultDescription = strResultDescription & "- Compore Number Result Fail | Step Name : " & strStepName & " | Step Detail : " & strDetail & vbNewLine	
		strResultStatus="Fail"
		FW_CaptureScreenShot("")
	End If
'*******************************************

End Function

'********** Export Report to Doc **********
Public Function FW_ExportReportToDoc()
	Dim objWord
	Set objWord = CreateObject("Word.Application")
	objWord.Visible = False
	'strLocalCurrTestPath & "\" & TestName &"\DOC\Report.docx"
	originalFileExport = strResultFileName & ".docx"
	Set fso = CreateObject("Scripting.FilesystemObject")
	If fso.FileExists(PATH_EXPORT_RESULT & "Report.docx") Then
		Set objDoc = objWord.Documents.Open (strLocalCurrentTestPath & "\" & batchName & "\DOC\Report.docx")
		objWord.Selection.End
		Key 6,0
		Set objSelection = objWord.Selection
		Set objShape = objDoc.InlineShapes
	Else
		Set objDoc = objWord.Documents.Add()
		Set objSelection = objWord.Selection
		Set objShape = objDoc.InlineShapes
	End If
	
	intRow = DataTable.GetSheet("TestStep").GetRowCount
	
	For intRowIndex = 1 to intRow

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strTestStep = Trim(DataTable.Value("LogStep","TestStep"))

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strStatus = Trim(DataTable.Value("LogStatus","TestStep"))

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		'Myfile = Trim(DataTable.Value("CapScreen","TestStep"))
		myFile = DataTable.Value("CaptureScreenShot","TestStep")

		DataTable.GetSheet("TestStep").SetCurrentRow(intRowIndex)
		strStartTime = DataTable.Value("StartTime", "TestStep")
 		strEndTime = DataTable.Value("EndTime", "TestStep")

		Set fso = Nothing
		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Script ID : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strTestStep)
		objSelection.TypeParagraph()
	
		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Step : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStatus)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Start Time is : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStartTime)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "End Time is : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strEndTime)
		objSelection.TypeParagraph()

		objSelection.ParagraphFormat.SpaceAfter = 0
		objSelection.Font.Name = "Arial"
		objSelection.Font.Size = "18"
		objSelection.Font.Bold = True
		objSelection.TypeText "Test Step : "
		objSelection.Font.Bold = False
		objSelection.TypeText cstr(strStatus)
		objSelection.TypeParagraph()
		objSelection.InlineShapes.AddPicture(myFile)
		objSelection.TypeParagraph()

'		intRow = DataTable.GetSheet("CapScreen").GetRowCount
'		
'		For Iterator = 1 To intRow
'			DataTable.GetSheet("CapScreen").SetCurrentRow(intRowIndex)
'			Myfile = Trim(DataTable.Value("No","CapScreen"))
'			objSelection.InlineShapes.AddPicture(Myfile)
'		Next
	Next
	
	objDoc.SaveAs2(strResultFileName & "Report.docx")
	objDoc.Close
	objWord.Quit
	
End Function
'******************************************

'********** Transaction Start **********
Public Function FW_TransactionStart(strTransaction)
	'Call StartTime(strStartTime)
	Services.StartTransaction strTransaction
	'Call FW_WriteLog("FW_TransactionStart StartTime : " & strStartTime & ", StartTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeStart(strStartTime, strTransaction)
End Function
'***************************************

'********** Transaction End **********
Public Function FW_TransactionEnd(strTransaction)
	Call EndTime(strEndTime)
	Services.EndTransaction strTransaction
	Call FW_WriteLog("FW_TransactionEnd EndTime : " & strEndTime & ", EndTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeEnd(strEndTime)
	If strResultStatus = "Fail" Then
		Call FW_CloseWebBrowserChromeWhenError()
		Call FW_CloseWebBrowserFireFoxWhenError()
		Call FW_CloseWebBrowserIEWhenError()
		EXECUTE_STATUS = "FAIL"
		Call FW_WriteLog("ExitAction", "")
		Call FW_ExitIteration(strResultDescription)
	End If
End Function
'*************************************

'********** Mainframe Transaction Start **********
Public Function MF_TransactionStart(strTransaction)
	Call StartTime(strStartTime)
	Services.StartTransaction strTransaction
	Call FW_WriteLog("MF_TransactionStart StartTime : " & strStartTime & ", StartTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeStart(strStartTime, strTransaction)
End Function
'***************************************

'********** Mainframe Transaction End **********
Public Function MF_TransactionEnd(strTransaction, objTeWindow, objTeScreen)
	Call EndTime(strEndTime)
	Services.EndTransaction strTransaction
	Call FW_WriteLog("MF_TransactionStart EndTime : " & strEndTime & ", EndTransaction : " & strTransaction, "")
	Call SaveLogTransactionTimeEnd(strEndTime)
	If strResultStatus = "Fail" Then
		TeWindow(objTeWindow).TeScreen(objTeScreen).SendKey TE_CLEAR
		EXECUTE_STATUS = "FAIL"
		Call FW_WriteLog("ExitAction", "")
		Call FW_ExitIteration(strResultDescription)
	End If
End Function
'*************************************

Public Function SaveLogTransactionTimeStart(strStartTime, strTransaction)
	On Error Resume Next 
	iRowCount = Datatable.GetSheet("TestStep").GetRowCount
 	Datatable.GetSheet("TestStep").SetCurrentRow(iRowCount + 1)
 	DataTable.Value("FunctionNo","TestStep") = functionNo
 	DataTable.Value("StepName","TestStep") = RUNNING_STEP & "# " & strTransaction
 	DataTable.Value("StartTime","TestStep") = "'" & strStartTime
    'DataTable.Value("EndTimeTransactionName","LogTransactionTime") = strEndTime
End Function  

Public Function SaveLogTransactionTimeEnd(strEndTime)
	On Error Resume Next 
	iRowCount = Datatable.GetSheet("TestStep").GetRowCount
 	Datatable.GetSheet("TestStep").SetCurrentRow(iRowCount)
 	'DataTable.Value("TransactionName","LogTransactionTime") = strTransaction
 	'DataTable.Value("StartTimeTransactionName","LogTimeTransaction") = StrStartTime
 	DataTable.Value("EndTime","TestStep") = "'" & strEndTime
 	RUNNING_STEP = RUNNING_STEP + 1
End Function

Public Function StartTime(strStartTime)
	'StrStartTime = Time()
	t = Timer
	temp = Int(t)

	milliseconds = Int((t - temp) * 1000)

	seconds = temp Mod 60
	temp    = Int(temp / 60)
	minutes = temp Mod 60
	hours   = Int(temp / 60)

	'Print "Hours is "&" " & Hours &"| Minutes is "&" " & Minutes &"| Seconds is "&" " & Seconds &"| Milliseconds is "&" " & Milliseconds

	strStartTime = String(2 - Len(hours), "0") & hours & ":"
	strStartTime = strStartTime & String(2 - Len(minutes), "0") & minutes & ":"
	strStartTime = strStartTime & String(2 - Len(seconds), "0") & seconds & "."
	strStartTime = strStartTime & String(4 - Len(milliseconds), "0") & milliseconds
	Print "StartTime : " & strStartTime
End Function  

Public Function EndTime(strEndTime)
	'StrEndTime = Time()
	'MsgBox startTime

	t = Timer
	temp = Int(t)

	milliseconds = Int((t-temp) * 1000)

	seconds = temp Mod 60
	temp    = Int(temp / 60)
	minutes = temp Mod 60
	hours   = Int(temp / 60)

	'Print "Hours is "&" " & Hours &"| Minutes is "&" " & Minutes &"| Seconds is "&" " & Seconds &"| Milliseconds is "&" " & Milliseconds

	strEndTime = String(2 - Len(hours), "0") & hours & ":"
	strEndTime = strEndTime & String(2 - Len(minutes), "0") & minutes & ":"
	strEndTime = strEndTime & String(2 - Len(seconds), "0") & seconds & "."
	strEndTime = strEndTime & String(4 - Len(milliseconds), "0") & milliseconds
	Print "EndTime : " & strEndTime
End Function  

Public Function SaveStatus(strStatus, myFile, strBusinessError, strTechnicalError)
	On Error Resume Next 
	getRowCount = Datatable.getSheet("TestStep").getRowCount
 	Datatable.getSheet("TestStep").SetCurrentRow(getRowCount)
 	DataTable.Value("Status","TestStep") = strStatus
 	DataTable.Value("CaptureScreenShot","TestStep") = myFile
 	DataTable.Value("BusinessError","TestStep") = strBusinessError
 	DataTable.Value("TechnicalError","TestStep") = strTechnicalError
End Function

Public Function Killservicejava()
	On error resume next    
	strComputer = "."
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = 'java.EXE'")
	For Each objProcess in colProcessList
	objProcess.Terminate()
	Next
End Function

Public Function CopyResultToETM(strWorkspace, strAppName, strTestName, outputFileData)

	strDestinationFolder = strWorkspace & "TestScript\" & strAppName & "\" & strTestName & "\AdapterRunRes\Report\"
	Set oFSO = CreateObject("Scripting.FileSystemObject")
	
	If oFSO.FolderExists(strDestinationFolder) Then
		strDeletePath = Mid(strDestinationFolder, 1, (len(strDestinationFolder) - 1))
		'Print "Delete Foloder Path --> " & strDeletePath
      	oFSO.DeleteFolder strDeletePath
    End If
	
	tmpArr = Split(strDestinationFolder, "\")
	tmpPath = tmpArr(0)
	For i = 1 To UBound(tmpArr)
	    If Not oFSO.FolderExists(tmpPath) Then
	      oFSO.CreateFolder tmpPath
	    End If
    	tmpPath = tmpPath & "\" & tmpArr(i)
	Next
	
	Call GetCurrentDateTimeFormat
	outputDestinationFileData = Replace(outputFileData, ".xlsx", "")
	outputDestinationFileData = outputDestinationFileData & "_" & generateFormatDate & ".xlsx" 
	
	oFSO.CopyFile outputFileData, outputDestinationFileData
	
	'oFSO.CopyFile outputFileData, strDestinationFolder, TRUE
	oFSO.CopyFile outputDestinationFileData, strDestinationFolder, TRUE
	'Print "Copy Results " & outputFileData & " --> " & strDestinationFolder
	
	If CAPTURE_SCREEN_SHOT_STATUS = "Y" Then
		strImagePath = Mid(strResultPathExecution, 1, (len(strResultPathExecution) - 1))
		'Print "Path --> " & strImagePath
		
		'Zip File
		Set ODirectory = DotNetFactory.CreateInstance("System.IO.Directory","System")            
        If ODirectory.Exists(strImagePath) Then
            Set OZipObject = DotNetFactory.CreateInstance("System.IO.Compression.ZipFile","System.IO.Compression.FileSystem")
            OZipObject.CreateFromDirectory strImagePath, strImagePath & ".zip"
            Set OZipObject = Nothing
         End IF
		Set ODirectory = Nothing
		
		'Copy File
		oFSO.CopyFile strImagePath & ".zip", strDestinationFolder, TRUE
		'Print "Copy Images " & strImagePath & ".zip" & " --> " & strDestinationFolder
		'Delete File
		oFSO.DeleteFile strImagePath & ".zip", TRUE
		'Print "Delete Images " & strImagePath & ".zip"
		
	End If
	
	Set oFSO = Nothing
	
End Function


Public Function GetDateFormat(strDate)
	FORMAT_DATE = strDate
	positionDate = Instr(1, FORMAT_DATE, "/", 1)
	If positionDate <> 0 Then
		dateArrayList = Split(strDate,"/")
		FORMAT_DATE = dateArrayList(0) & dateArrayList(1) & dateArrayList(2)
	End If
	'Print "FORMAT_DATE --> " & FORMAT_DATE
End Function

'********* Get Current Date Time Format [YYYYMMDD_HHMMSS] *********
Public Function GetCurrentDateTimeFormat
	GetCurrentDateTimeFormat = 0
	
	If Len(Month(Now)) = 1 Then  
		currentMonth = 0 & Month(Now)   
	Else  
		currentMonth = Month(Now) 
	End If
	
	If Len(Day(Now)) = 1 Then 
		currentDay = 0 & Day(Now) 
	Else 
		currentDay = Day(Now) 
	End If
	
	If Len(Hour(Now)) = 1 Then 
		currentHour = 0 & Hour(Now) 
	Else 
		currentHour = Hour(Now) 
	End If
	
	If Len(Minute(Now)) = 1 Then    
		currentMinute = 0 & Minute(Now)    
	Else    
		currentMinute = Minute(Now) 
	End If
	
	If Len(Second(Now)) = 1 Then    
		currentSecond = 0 & Second(Now)    
	Else    
		currentSecond = Second(Now) 
	End If
	GetCurrentDateTimeFormat = Year(Now) & currentMonth & currentDay & "_" & currentHour & currentMinute & currentSecond  
	generateFormatDate = GetCurrentDateTimeFormat
End Function
'******************************************************************
