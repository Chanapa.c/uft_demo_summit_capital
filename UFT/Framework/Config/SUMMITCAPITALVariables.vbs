'#######################################################################################
'# Script Name				: 
'# Script Description 		: This script contents global variables and file variables. 
'# Version Information		: Phase	1.0 
'# Client Name				: 
'# Date Created  			: 16/06/2020
'# Author					: 
'# Function					: 	 
'#######################################################################################
Option Explicit
Public fso					'	Parameter Create Object for Manage File
Public objExcel
Public objWorkBook
Public objWorkSheet
Public objWorkSheetDeposit
Public objWorkSheetBatch
Public objWorkSheetInstrument
Public objChildExcel
Public objChWorkBook
Public objChWorkSheet

Public myFile
Public functionNo
Public dataRow
Public startExecuteDate
Public startExecuteTime
Public stopExecuteTime
Public durationTime

'Public StartTimeExecuted
'Public StopTimeExecuted
'Public MyMstResultFile
'Public Action
'Public Status 
'Public strBrowser
'Public strPage
'Public strFrame
'public strMsgTxt
'public strFileName
'public strSheetName
'public strChildSheetName
'Public gstrOutputFile

'Navigation
Public objBrowser			'	Create Object Browser
Public objPage				'	Create Object Page
Public objWebTable			'	Create Object WebTable
Public strWebLink			'	String Define Object WebLink
Public strWebButton			'	String Define Object WebButton
Public strCellData			'	WebTable Get Cell Data

'Iteration Variables
Public ItrCnt
Public InstCnt

'Constant Declaration for Wait Property
Const WAIT_SHORT = 2
Const WAIT_MEDIUM = 5
Const WAIT_LONG = 10
Const WAIT_HALF = 30
Const WAIT_DOUBLE = 60

'Constant Declaration for General
Public strResultPathFolderRowExecution  'Parameter for Create Folder SnapShot & Define Result name
Public strResultPathExecution			'Parameter for Create Folder SnapShot & Define Result name
Public strResultFileExecution			'Parameter for Create Folder SnapShot & Define Result name
Public strResultPathTestCase			'Parameter for Create Folder TestCase Result & SnapShot
Public strResultFileName				'Parameter for Create File Result Export from DataTable
Const DELIMITER = "|"

'Excel Config
'Const Path_TestData="D:\Automate\WebTours\TestData\"
'Const PATH_IMPORT_DATA = "C:\Jenkins\workspace\TRAINING\UFT_FRAMEWORK\Framework\Data"
Public TEST_DIR
TEST_DIR = Environment.Value("TestDir")
'Print "TEST_DIR --> " & TEST_DIR
Public WORKSPACE
WORKSPACE = Replace(CStr(TEST_DIR),"\Framework\TestController\Mainframe","\Framework\")
'Print "WORKSPACE --> " & WORKSPACE
Public PATH_IMPORT_DATA
PATH_IMPORT_DATA = Cstr(WORKSPACE) & "Data\Mainframe"
Const SHEET_TEST_CONTROLLER = "TestController"
Const FILE_TEST_DATA = "TestController.xlsx"
'Const PATH_EXPORT_RESULT = "C:\Jenkins\workspace\TRAINING\UFT_FRAMEWORK\Framework\Result\"
public PATH_EXPORT_RESULT
PATH_EXPORT_RESULT =  Cstr(WORKSPACE) & "Result\Mainframe\"
Const L_FILENAME_RESULT = "_Result"
Const R_FILENAME_RESULT = "SIUpgrade_"
'Const StrTemp= strFolderpath & "\" & "Temp"
'Const ExcelType=".xlsx"

'Constant Declaration for Excel Result
Const RESULT_STATUS = "Status"					'string Column DataTable Result Status
Const RESULT_DESCRIPTION = "Description"		'string Column DataTable Result Description
Public strResultDescription
Public strResultStatus

Public EXECUTE_VERSION
Public EXECUTE_STATUS

'------------------------------ Scenario Control Started -----------------------------------------'
Public strScenario
Public strTag
'------------------------------ Scenario Control End -----------------------------------------'

'------------------------------ MCN (Get Content log from Mobile SMS) Application Parameter Started -----------------------------------------'
'	URL 
Const WebURL_MCN = ""

'------------------------------ User Modification End ---------------------------------------------'
Public dataTag
Public dataColumn
Public RUNNING_STEP
Public RUNNING_CAPTURE
Public RUNNING_CAPTURE_PAGE
Public MF_VERIFY
Public MF_EXIT_TEST
Public CAPTURE_SCREEN_SHOT_STATUS
Public FOLDER_ROW
Public MF_OUTPUT_ROW
Public MF_CAPTURE_NAME
Public FORMAT_DATE
Public generateFormatDate

Public strTempRow
Public strStatus
Public strSuccess
Public strBusinessError
Public strTechnicalError

Public obj
Public errorCode
Public errorDescription
