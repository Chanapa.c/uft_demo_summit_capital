﻿'********************************************************************************
'Initial Function Library
'********************************************************************************
Dim INIT_TEST_DIR, INIT_TEST_NAME, INIT_WORKSPACE
INIT_TEST_DIR = Environment.Value("TestDir")
'Print "INIT_TEST_DIR --> " & INIT_TEST_DIR

INIT_TEST_NAME = parameter.Item("TEST_NAME")
If INIT_TEST_NAME = "" Then
	INIT_TEST_NAME = Environment.Value("TestName")
End If
Print "INIT_TEST_NAME --> " & INIT_TEST_NAME

APP_NAME = parameter.Item("APP_NAME")
If APP_NAME = "" Then
	APP_NAME = "SummitCapital_WEB"
End If
Print "APP_NAME --> " & APP_NAME

OUTPUT_SHEET_NAME = parameter.Item("OUTPUT_SHEET_NAME")
If OUTPUT_SHEET_NAME = "" Then
	OUTPUT_SHEET_NAME = "OUTPUT"
End If
Print "OUTPUT_SHEET_NAME --> " & OUTPUT_SHEET_NAME

INIT_WORKSPACE = parameter.Item("WORKSPACE")
If INIT_WORKSPACE = "" Then
	INIT_WORKSPACE = Replace(CStr(INIT_TEST_DIR),"\Framework\TestScript\" & APP_NAME & "\" & INIT_TEST_NAME ,"\Framework\")
End If
Print "INIT_WORKSPACE --> " & INIT_WORKSPACE

'Load Variables
LoadFunctionLibrary(INIT_WORKSPACE & "Config\SUMMITCAPITALVariables.vbs")
'Load General Functions
LoadFunctionLibrary(INIT_WORKSPACE & "Library\General\GeneralFunctions.vbs")
'Load Mainframe Functions
LoadFunctionLibrary(INIT_WORKSPACE & "Library\Business\" & APP_NAME & "\SummitCapitalFunctionsWeb.vbs")
'Load Object Repository
RepositoriesCollection.Add INIT_WORKSPACE & "ObjectRepository\" & APP_NAME & "\SUMMITCAPITAL_CAL_CREDIT_Repository.tsr" 

'Read Excel Configuration Data
inputFileConfigData = INIT_WORKSPACE & "Data\" & APP_NAME & "\SummitCapitalConfiguration.xlsx"

Call FW_GetConfigData(inputFileConfigData, "Template")

'Read Path Data 
inputFileTempData = Trim(dataColumn(INIT_TEST_NAME))
inputFileArrayData = Split(inputFileTempData, "|")
inputFileData = inputFileArrayData(0)
MF_VERIFY = inputFileArrayData(1)
MF_EXIT_TEST = "N"
MF_OUTPUT_ROW = 1
strTempRow = 1

inputSheetData = "INPUT"
outputFileData = inputFileData
outputSheetData = OUTPUT_SHEET_NAME
'Call FW_WriteLog("REGISTER_NOW", INIT_TEST_NAME)
RegisterFileData = outputFileData
'Call FW_WriteLog("LQ InterInquiryFileData", lqInterInquiryFileData)

'Create OUTPUT Header Column
DataTable.AddSheet(outputSheetData)
DataTable.GetSheet(outputSheetData).AddParameter "TS_STATUS" ,""
DataTable.GetSheet(outputSheetData).AddParameter "TS_MESSAGE" ,""
DataTable.GetSheet(outputSheetData).AddParameter "TS_START_TIME" ,""
DataTable.GetSheet(outputSheetData).AddParameter "TS_END_TIME" ,""
DataTable.GetSheet(outputSheetData).AddParameter "CAPTURE_SCREEN_SHOT" ,""
DataTable.GetSheet(outputSheetData).AddParameter "SELECT_MENU" ,""
DataTable.GetSheet(outputSheetData).AddParameter "SUMMITCAPITAL_URL" ,""
DataTable.GetSheet(outputSheetData).AddParameter "CAR_PRICE" ,""
DataTable.GetSheet(outputSheetData).AddParameter "DOWN_PAYMENT" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INSTALLMENT_DURATION" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INTEREST_RATE_PER_MONTH" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INSTALLMENT_AMOUNT_PER_MONTH" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INSTALLMENT_PER_MONTH" ,""
DataTable.GetSheet(outputSheetData).AddParameter "DOWN_PAYMENT_2" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INSTALLMENT_DURATION_2" ,""
DataTable.GetSheet(outputSheetData).AddParameter "INTEREST_RATE_PER_MONTH_2" ,""
DataTable.GetSheet(outputSheetData).AddParameter "HIREPURCHASE_CREDIT" ,""

'Capture Screen Shot
CAPTURE_SCREEN_SHOT_STATUS = inputFileArrayData(2)
CAPTURE_SCREEN_SHOT_PATH = inputFileArrayData(3)
RUNNING_CAPTURE = 1
''Call FW_WriteLog("CAPTURE_SCREEN_SHOT_STATUS", CAPTURE_SCREEN_SHOT_STATUS)
''Call FW_WriteLog("CAPTURE_SCREEN_SHOT_PATH", CAPTURE_SCREEN_SHOT_PATH)

'Create Capture When Capture Status = "Y"
If CAPTURE_SCREEN_SHOT_STATUS = "Y" Then
	strGetTime = GetCurrentDateTimeFormat
	strResultPathExecution = CAPTURE_SCREEN_SHOT_PATH & "\" & strGetTime & "\"
	''Call FW_WriteLog("CAPTURE_SCREEN_SHOT_PATH", strResultPathExecution)
	Call FW_CreateFolder(strResultPathExecution)
End If
'Capture Screen Shot

'Read Excel Get Total TestData
Call FW_GetTotalTestData(inputFileData, inputSheetData)
'Read Total TestData
totalTestData = Trim(dataColumn("TotalTestData"))
'Call FW_WriteLog("totalTestData", totalTestData)

WORKSPACE = INIT_WORKSPACE
For currentRow = 1 To totalTestData-1 Step 1
	'Call FW_WriteLog("Set CurrentRow", currentRow)
	tagData =  Trim(dataTag("TAG_" & currentRow))
	
	'Call FW_WriteLog("TagData", tagData)
	'Call FW_WriteLog("Exit Test --> ", SC_EXIT_TEST)
	
	If MF_EXIT_TEST = "N" Then
		If tagData = "Run" Then
		
		startRow = MF_OUTPUT_ROW
		Call FW_StartExecute(outputSheetData, startRow)
		
		'Create Capture Sub Folder When Capture Status = "Y"
			RUNNING_CAPTURE = 1
			If CAPTURE_SCREEN_SHOT_STATUS = "Y" Then
				Call FW_AddZeroFolderRow(currentRow + 1)
				''Call FW_WriteLog("FOLDER ROW", FOLDER_ROW)
				strResultPathFolderRowExecution = strResultPathExecution & FOLDER_ROW & "\"
				''Call FW_WriteLog("CAPTURE_SCREEN_SHOT_PATH_FOLDER_ROW", strResultPathFolderRowExecution)
				Call FW_CreateFolder(strResultPathFolderRowExecution)
			End If
			'Capture Screen Shot

			'Call FW_WriteLog("Run CurrentRow", currentRow)
			'Read Excel TestData
			Call FW_GetTestData(inputFileData, inputSheetData, currentRow)
			
			'Get Data
			selectMenuData = Trim(dataColumn("SELECT_MENU"))
			
			Select Case selectMenuData
				Case "CAL_CREDIT"
					RunAction "CAL_CREDIT", oneIteration, inputFileData, currentRow, WORKSPACE, outputSheetData
			End Select
			
			Call FW_StopExecute(outputSheetData, startRow)
			'Export ข้อมูลลง Sheet OUPOUT
			Datatable.ExportSheet outputFileData, outputSheetData
			MF_OUTPUT_ROW = MF_OUTPUT_ROW + 1
			
		Else
			'Call FW_WriteLog("No Run Skip CurrentRow", currentRow)
		End If
	End If
	Print "currentRow -->" & currentRow
	Print "totalTestData -->" & totalTestData
Next

'Call FW_WriteLog("End Account Inquiry","OK")

'Print "StartExecute --> " &  startExecuteDate & " " & startExecuteTime
'Call FW_StopExecute()

'ExitTestIteration
